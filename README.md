# Molecule superstructures for computer-aided molecular and process design

This repository contains all scripts required to reproduce the results in [Rehner et al. (2023)](https://pubs.rsc.org/en/content/articlelanding/2023/ME/D2ME00230B).
In particular, this repository contains
- The process model for the organic Rankine cycle with optional recuperation
- The Rust script that runs the different optimizations
- A Python Notebook for postprocessing

The more generic implementations of the molecule superstructures and the surrounding CAMPD framework are published in the Rust crate [`feos-campd`](https://crates.io/crates/feos-campd).

In general, the Rust code and the Python scripts should run on every major platform. In practice, the code was developed on Linux and in some parts (the bindings to Knitro and the postprocessing notebook) file paths might need to be adjusted to make it work on Windows and macOS.

## Installation
To run the optimizations themselves, you need the current [Rust](https://www.rust-lang.org/) compiler and [Artelys Knitro](https://www.artelys.com/solvers/knitro/). Due to the large computation times and the inaccessibility of Knitro, the optimization results are published on this repository as well.

The postprocessing is done in Python (3.9.10). Aside from standard Python libraries like `numpy` and `matplotlib` you also need to have [`rdkit`](https://anaconda.org/conda-forge/rdkit) installed.

## Fluid ranking
To calculate a ranking of the optimal working fluids for an ORC specified in [`input/orc_recuperator.json`](input/orc_recuperator.json) run either of
```
cargo run --release calculate pcsaft 50
cargo run --release calculate gc-pcsaft 50
```
depending on which property prediction method you want to use. The `50` specifies the number of fluids in the ranking and can be chosen arbitrarily. The results of the calculation are saved in either [`results/pcsaft.json`](results/pcsaft.json) or [`results/gc-pcsaft.json`](results/gc-pcsaft.json).

## Comparison to fitted PC-SAFT parameters
To calculate the comparison values using PC-SAFT with fitted parameters, run either of
```
cargo run --release compare pcsaft
cargo run --release compare gc-pcsaft
```
The results are saved in [`results/pcsaft_comparison.json`](results/pcsaft_comparison.json) and [`results/gc-pcsaft_comparison.json`](results/gc-pcsaft_comparison.json). The script might crash due to the PubChem API timing out. In this case just try rerunning it after a couple of minutes.

## Pareto optimization
The multiobjective optimization can be run using
```
cargo run --release pareto
```
The results are saved in [`results/pareto.json`](results/pareto.json). To generate the input for the pareto plot, run
```
cargo run --release pareto_plot
```
This command extracts the relevant data from the more comprehensive [`results/pareto.json`](results/pareto.json) and saves them to [`results/pareto_front.json`](results/pareto_front.json). It also calculates the T-s diagrams for three given temperatures and saves those results in [`results/pareto_plots.json`](results/pareto_plots.json).

## Postprocessing
In [`results/postprocess.ipynb`](results/postprocess.ipynb), the JSON files in the `results` directory are used to generate the plots shown in the manuscript. To save the figures, set the output directory
```python
output_directory = None
```
in the first cell.
