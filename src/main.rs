use feos::pcsaft::PcSaftParameters;
use feos_campd::process::*;
use feos_campd::*;
use feos_core::parameter::{IdentifierOption, Parameter, SegmentRecord};
use feos_core::{EosResult, PhaseDiagram};
use pubchem::{Compound, CompoundProperty::*};
use quantity::si::*;
use rayon::iter::once;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::BufWriter;
use std::rc::Rc;
use std::time::Duration;
use std::{env, thread};

pub use orc_superstructure::{OrganicRankineCycleRecuperator, OrganicRankineCycleSuperStructure};
mod orc_superstructure;

const X0: [f64; 6] = [0.6, -1.5, -0.2, 0.8, 0.0, 1.0];
const OPTIONS: Option<&'static str> = Some("input/options_12.opt");
const OPTIONS_NLP: Option<&'static str> = Some("input/options_12_NLP.opt");

fn main() -> EosResult<()> {
    let args: Vec<String> = env::args().collect();
    let command: &str = &args[1];
    match command {
        "calculate" => calculate(&args[2], args[3].parse().unwrap()),
        "pareto" => pareto(),
        "compare" => compare(&args[2]),
        "pareto_plot" => pareto_plot(),
        _ => unimplemented!(),
    }
}

fn calculate(model: &str, n_solutions: usize) -> EosResult<()> {
    match model {
        "pcsaft" => calculate_(
            model,
            PcSaftPropertyModel::new("input/sauer2014_homo_joback.json")?,
            n_solutions,
        ),
        "gc-pcsaft" => calculate_(
            model,
            GcPcSaftPropertyModel::new("input/sauer2014_hetero_joback.json")?,
            n_solutions,
        ),
        _ => unimplemented!(),
    }
}

fn calculate_<R: PropertyModel<SegmentAndBondCount> + Sync + Serialize>(
    model: &str,
    property_model: R,
    n_solutions: usize,
) -> EosResult<()> {
    let orc = OrganicRankineCycleSuperStructure::from_json("input/orc_recuperator.json")?;
    let mut problem = MetaOptimizationProblem::new(8, property_model, orc);
    problem.solve_knitro(&X0, n_solutions, OPTIONS);
    problem.to_json(&format!("results/{model}.json"))?;
    Ok(())
}

fn compare(model: &str) -> EosResult<()> {
    rayon::ThreadPoolBuilder::new()
        .num_threads(8)
        .build_global()
        .unwrap();

    match model {
        "pcsaft" => compare_::<PcSaftPropertyModel>(model),
        "gc-pcsaft" => compare_::<GcPcSaftPropertyModel>(model),
        _ => unimplemented!(),
    }
}

fn compare_<R>(model: &str) -> EosResult<()>
where
    for<'a> R: Deserialize<'a>,
{
    let problem: MetaOptimizationProblem<R, OrganicRankineCycleRecuperator> =
        MetaOptimizationProblem::from_json(&format!("results/{model}.json"))?;

    let comparison = problem
        .solutions
        .iter()
        .map(|(c, r)| ComparisonResult::new(&problem, c, r.clone()))
        .collect::<Result<Vec<_>, _>>()?;

    serde_json::to_writer_pretty(
        BufWriter::new(File::create(&format!("results/{model}_comparison.json")).unwrap()),
        &comparison,
    )
    .unwrap();

    Ok(())
}

#[derive(Clone, Serialize)]
struct ComparisonResult {
    chemical: String,
    smiles: String,
    iupac_name: String,
    result: OptimizationResult,
    target_pcsaft: Option<f64>,
}

impl ComparisonResult {
    fn new<R, P: ProcessModel + Clone>(
        problem: &MetaOptimizationProblem<R, P>,
        chemical: &str,
        result: OptimizationResult,
    ) -> EosResult<Self> {
        let molecule = &problem.molecules[chemical];
        let process = &problem.process;

        // Obtain properties from PubChem
        let smiles = molecule.smiles(result.y.clone());
        let mut prop = None;
        for i in 0..20 {
            prop = Compound::with_smiles(&smiles)
                .properties(&[IUPACName, CanonicalSMILES])
                .ok();
            if prop.is_some() {
                break;
            }
            println!("{i}");
            thread::sleep(Duration::from_millis(2000));
        }
        let prop = prop.unwrap();
        let smiles = prop.canonical_smiles.unwrap();
        let iupac_name = prop.iupac_name.unwrap();

        // Calculate Joback parameter
        let y = result.y.iter().map(|y| *y as f64).collect();
        let chemical_record = molecule.build(y);
        let segment_records = SegmentRecord::from_json("input/sauer2014_homo_joback.json")?;
        let parameter =
            PcSaftParameters::from_segments(vec![chemical_record], segment_records, None)?;
        let pure_record = &parameter.pure_records[0];
        let joback = pure_record.ideal_gas_record.clone();

        // Calculate PC-SAFT
        let target_pcsaft = PcSaftParameters::from_json(
            vec![&iupac_name],
            "input/25052022_pure_parameters.json",
            None,
            IdentifierOption::IupacName,
        )
        .ok()
        .map(|mut parameter| {
            let mut pure_record = parameter.pure_records.pop().unwrap();
            pure_record.ideal_gas_record = joback;

            let property_model = PcSaftFixedPropertyModel::new(pure_record);
            let molecule = FixedMolecule;
            let mut problem = OptimizationProblem::new(molecule, property_model, process.clone());
            problem
                .solve_knitro(&result.x[0..5], 1, OPTIONS_NLP)
                .unwrap();
            let eos = Rc::new(problem.property_model.build_eos(()).unwrap());
            let result_pc_saft = &problem.solutions[0];
            let (_, target_pcsaft, _) = process.solve(&eos, &result_pc_saft.x).unwrap();
            target_pcsaft
        });
        Ok(Self {
            chemical: chemical.to_string(),
            smiles,
            iupac_name,
            result,
            target_pcsaft,
        })
    }
}

fn pareto() -> EosResult<()> {
    rayon::ThreadPoolBuilder::new()
        .num_threads(8)
        .build_global()
        .unwrap();

    let property_model = GcPcSaftPropertyModel::new("input/sauer2014_hetero_joback.json")?;
    let orc = OrganicRankineCycleSuperStructure::from_json("input/orc_recuperator.json")?;
    let pareto_front = SIArray1::linspace(100.0 * CELSIUS, 290.0 * CELSIUS, 191)?
        .to_vec()
        .into_par_iter()
        .map(Some)
        .chain(once(None))
        .map(|t| {
            let mut orc = orc.clone();
            orc.min_heat_source_outlet_temperature = t;
            let mut problem = MetaOptimizationProblem::new(8, property_model.clone(), orc);
            problem.find_best_knitro(&X0, 2, OPTIONS);
            problem
        })
        .collect::<Vec<_>>();
    serde_json::to_writer_pretty(
        BufWriter::new(File::create("results/pareto.json").unwrap()),
        &pareto_front,
    )
    .unwrap();
    Ok(())
}

fn pareto_plot() -> EosResult<()> {
    let problems: Vec<
        MetaOptimizationProblem<GcPcSaftPropertyModel, OrganicRankineCycleSuperStructure>,
    > = serde_json::from_reader(File::open("results/pareto.json").unwrap()).unwrap();

    let mut plot_data = Vec::new();
    for i in [0, 100, 190] {
        let problem = &problems[i];
        let (chemical, results) = &problem.solutions[0];
        let y = results.y.iter().map(|y| *y as f64).collect();
        let chemical_record = problem.molecules[chemical].build(y);
        let eos = Rc::new(problem.property_model.build_eos(chemical_record)?);
        let (process, _, _) = problem.process.solve(&eos, &results.x)?;
        let plot = process.plot()?;
        let dia = PhaseDiagram::pure(&eos, 50.0 * CELSIUS, 300, None, Default::default())?;
        let dia_liquid = ProcessPlot {
            entropy: dia.liquid().specific_entropy(),
            temperature: dia.liquid().temperature(),
            utility_temperature: None,
        };
        let dia_vapor = ProcessPlot {
            entropy: dia.vapor().specific_entropy(),
            temperature: dia.vapor().temperature(),
            utility_temperature: None,
        };
        plot_data.push((plot, dia_liquid, dia_vapor));
    }
    serde_json::to_writer_pretty(
        BufWriter::new(File::create("results/pareto_plots.json").unwrap()),
        &plot_data,
    )
    .unwrap();

    pareto_front(problems)
}

fn pareto_front<R>(
    problems: Vec<MetaOptimizationProblem<R, OrganicRankineCycleSuperStructure>>,
) -> EosResult<()> {
    let pareto_front = problems
        .into_iter()
        .map(|mut problem| {
            let (chemical, result) = problem.solutions.pop().unwrap();
            let molecule = &problem.molecules[&chemical];
            let smiles = molecule.smiles(result.y);
            (
                chemical,
                smiles,
                result.target,
                problem.process.min_heat_source_outlet_temperature,
                result.x[5],
            )
        })
        .collect::<Vec<_>>();

    let smiles: HashSet<_> = pareto_front
        .iter()
        .map(|(_, smiles, _, _, _)| smiles)
        .collect();
    let iupac_names: HashMap<_, _> = smiles
        .into_iter()
        .map(|smiles| {
            (
                smiles,
                Compound::with_smiles(smiles)
                    .properties(&[IUPACName])
                    .unwrap()
                    .iupac_name
                    .unwrap(),
            )
        })
        .collect();

    let pareto_front = pareto_front
        .iter()
        .map(|(chemical, smiles, target, temperature, recuperator)| {
            (
                chemical,
                &iupac_names[&smiles],
                smiles,
                target,
                temperature.map(|t| t / CELSIUS),
                recuperator,
            )
        })
        .collect::<Vec<_>>();
    serde_json::to_writer_pretty(
        BufWriter::new(File::create("results/pareto_front.json").unwrap()),
        &pareto_front,
    )
    .unwrap();
    Ok(())
}
