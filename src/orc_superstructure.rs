use feos_campd::process::{Isobar, Process, ProcessModel, ProcessState, Utility};
use feos_core::parameter::ParameterError;
use feos_core::{
    Contributions, DensityInitialization, EosResult, EquationOfState, MolarWeight, State,
};
use quantity::si::*;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::rc::Rc;

const BIG_M: f64 = 500.0;

#[derive(Serialize, Deserialize)]
pub struct OrganicRankineCycleSuperStructureJSON {
    heat_source: Utility,
    #[serde(rename = "min_heat_source_outlet_temperature [°C]")]
    min_heat_source_outlet_temperature: Option<f64>,
    isentropic_turbine_efficiency: f64,
    isentropic_pump_efficiency: f64,
    #[serde(rename = "min_abs_pressure [bar]")]
    min_abs_pressure: f64,
    min_red_pressure: f64,
    #[serde(rename = "max_abs_pressure [bar]")]
    max_abs_pressure: f64,
    max_red_pressure: f64,
    cooling: Utility,
    #[serde(rename = "min_temperature_difference_recuperator [K]")]
    min_temperature_difference_recuperator: f64,
    #[serde(rename = "min_heat_transfer_rate_recuperator [kW]")]
    min_heat_transfer_rate_recuperator: f64,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(from = "OrganicRankineCycleSuperStructureJSON")]
#[serde(into = "OrganicRankineCycleSuperStructureJSON")]
pub struct OrganicRankineCycleSuperStructure {
    heat_source: Utility,
    pub min_heat_source_outlet_temperature: Option<SINumber>,
    isentropic_turbine_efficiency: f64,
    isentropic_pump_efficiency: f64,
    min_abs_pressure: SINumber,
    min_red_pressure: f64,
    max_abs_pressure: SINumber,
    max_red_pressure: f64,
    cooling: Utility,
    min_temperature_difference_recuperator: SINumber,
    min_heat_transfer_rate_recuperator: SINumber,
}

impl From<OrganicRankineCycleSuperStructureJSON> for OrganicRankineCycleSuperStructure {
    fn from(orc: OrganicRankineCycleSuperStructureJSON) -> Self {
        Self {
            heat_source: orc.heat_source,
            min_heat_source_outlet_temperature: orc
                .min_heat_source_outlet_temperature
                .map(|t| t * CELSIUS),
            isentropic_turbine_efficiency: orc.isentropic_turbine_efficiency,
            isentropic_pump_efficiency: orc.isentropic_pump_efficiency,
            min_abs_pressure: orc.min_abs_pressure * BAR,
            min_red_pressure: orc.min_red_pressure,
            max_abs_pressure: orc.max_abs_pressure * BAR,
            max_red_pressure: orc.max_red_pressure,
            cooling: orc.cooling,
            min_temperature_difference_recuperator: orc.min_temperature_difference_recuperator
                * KELVIN,
            min_heat_transfer_rate_recuperator: orc.min_heat_transfer_rate_recuperator
                * KILO
                * WATT,
        }
    }
}

impl From<OrganicRankineCycleSuperStructure> for OrganicRankineCycleSuperStructureJSON {
    fn from(orc: OrganicRankineCycleSuperStructure) -> Self {
        Self {
            heat_source: orc.heat_source,
            min_heat_source_outlet_temperature: orc
                .min_heat_source_outlet_temperature
                .map(|t| t / CELSIUS),
            isentropic_turbine_efficiency: orc.isentropic_turbine_efficiency,
            isentropic_pump_efficiency: orc.isentropic_pump_efficiency,
            min_abs_pressure: orc.min_abs_pressure.to_reduced(BAR).unwrap(),
            min_red_pressure: orc.min_red_pressure,
            max_abs_pressure: orc.max_abs_pressure.to_reduced(BAR).unwrap(),
            max_red_pressure: orc.max_red_pressure,
            cooling: orc.cooling,
            min_temperature_difference_recuperator: orc
                .min_temperature_difference_recuperator
                .to_reduced(KELVIN)
                .unwrap(),
            min_heat_transfer_rate_recuperator: orc
                .min_heat_transfer_rate_recuperator
                .to_reduced(KILO * WATT)
                .unwrap(),
        }
    }
}

#[allow(clippy::too_many_arguments)]
impl OrganicRankineCycleSuperStructure {
    pub fn new(
        heat_source: Utility,
        min_heat_source_outlet_temperature: Option<SINumber>,
        isentropic_turbine_efficiency: f64,
        isentropic_pump_efficiency: f64,
        min_abs_pressure: SINumber,
        min_red_pressure: f64,
        max_abs_pressure: SINumber,
        max_red_pressure: f64,
        cooling: Utility,
        min_temperature_difference_recuperator: SINumber,
        min_heat_transfer_rate_recuperator: SINumber,
    ) -> Self {
        Self {
            heat_source,
            min_heat_source_outlet_temperature,
            isentropic_turbine_efficiency,
            isentropic_pump_efficiency,
            min_abs_pressure,
            min_red_pressure,
            max_abs_pressure,
            max_red_pressure,
            cooling,
            min_temperature_difference_recuperator,
            min_heat_transfer_rate_recuperator,
        }
    }

    pub fn from_json<P: AsRef<Path>>(file: P) -> Result<Self, ParameterError> {
        let reader = BufReader::new(File::open(file)?);
        Ok(serde_json::from_reader(reader)?)
    }
}

impl ProcessModel for OrganicRankineCycleSuperStructure {
    fn variables(&self) -> Vec<[Option<f64>; 2]> {
        vec![
            // Mass flow rate
            [Some(0.0), None],
            // pressure condenser
            [
                Some(self.min_red_pressure.ln()),
                Some(self.max_red_pressure.ln()),
            ],
            // pressure evaporator
            [
                Some(self.min_red_pressure.ln()),
                Some(self.max_red_pressure.ln()),
            ],
            // superheating
            [Some(0.0), Some(1.0)],
            // heat flow recuperator
            [Some(0.0), None],
        ]
    }

    fn binary_variables(&self) -> usize {
        1
    }

    fn constraints(&self) -> Vec<[Option<f64>; 3]> {
        let mut constraints = vec![
            // Pinch constraints
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            // Absolute pressure (bar)
            [Some(0.0), Some(1.0), None],
            [Some(0.0), Some(1.0), None],
            // Recuperator
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
        ];
        // Heat source outlet
        if self.min_heat_source_outlet_temperature.is_some() {
            constraints.push([Some(0.0), None, None]);
        }
        constraints
    }

    fn solve<E: EquationOfState + MolarWeight<SIUnit>>(
        &self,
        eos: &Rc<E>,
        x: &[f64],
    ) -> EosResult<(Process<E>, f64, Vec<f64>)> {
        // unpack variables
        let mwf = x[0] * KILOGRAM / SECOND;
        let p_cond_red = x[1].exp();
        let p_evap_red = x[2].exp();
        let t_min = self
            .min_heat_source_outlet_temperature
            .unwrap_or(self.cooling.temperature);
        let dt_sh = x[3] * (self.heat_source.temperature - t_min);
        let q_rec = x[4] * MEGA * WATT;
        let recuperator = x[5];

        // Calculate pressures
        let cp = State::critical_point(eos, None, None, Default::default())?;
        let p_crit = cp.pressure(Contributions::Total);
        let p_cond = p_cond_red * p_crit;
        let p_evap = p_evap_red * p_crit;

        // Calculate isobars
        let isobar_cond = Isobar::new(eos, p_cond);
        let isobar_evap = Isobar::new(eos, p_evap);

        // Initialize process
        let mut process = Process::new();

        // Calculate pump
        let feed = process.add_state(ProcessState::SinglePhase(
            Box::new(isobar_cond.saturated_liquid()?.clone()),
            Some(mwf),
        ));
        let pump = process.pump(feed, &isobar_evap, self.isentropic_pump_efficiency)?;

        // Calculate turbine
        let feed = process.add_state(ProcessState::new_pure_temperature_pressure(
            eos,
            isobar_evap.saturated_vapor()?.temperature + dt_sh,
            p_evap,
            Some(mwf),
            DensityInitialization::Vapor,
        )?);
        let turbine = process.turbine(feed, &isobar_cond, self.isentropic_turbine_efficiency)?;

        // Calculate Recuperator
        let [recuperator_liquid, recuperator_vapor] =
            process.recuperator(pump.out(), turbine.out(), &isobar_evap, &isobar_cond, q_rec)?;

        // Calculate evaporator
        let evaporator = process.evaporator(recuperator_liquid.out(), &isobar_evap, Some(dt_sh))?;
        process.add_utility(&evaporator, self.heat_source);

        // Calculate condenser
        let condenser = process.total_condenser(recuperator_vapor.out(), &isobar_cond, None)?;
        process.add_utility(&condenser, self.cooling);

        // Target
        let target = process.net_power().unwrap().to_reduced(KILO * WATT)?;

        // Pinch constraints
        let mut constraints = process.pinch_constraints();

        // Absolute pressure constraints
        constraints
            .push((p_cond - self.min_abs_pressure).to_reduced(p_evap - self.min_abs_pressure)?);
        constraints.push((p_evap - p_cond).to_reduced(self.max_abs_pressure - p_cond)?);

        // Recuperator constraints
        constraints.push(
            (process[turbine.out()].temperature()
                - process[recuperator_liquid.out()].temperature())
            .to_reduced(self.min_temperature_difference_recuperator)?
                + (1.0 - recuperator) * BIG_M
                - recuperator,
        );
        constraints.push(
            (process[recuperator_vapor.out()].temperature() - process[pump.out()].temperature())
                .to_reduced(self.min_temperature_difference_recuperator)?
                + (1.0 - recuperator) * BIG_M
                - recuperator,
        );
        constraints.push(q_rec.to_reduced(self.min_heat_transfer_rate_recuperator)? - recuperator);
        constraints
            .push(recuperator * BIG_M - q_rec.to_reduced(self.min_heat_transfer_rate_recuperator)?);

        // Heat source outlet constraint
        if let Some(t_out) = self.min_heat_source_outlet_temperature {
            constraints.push(
                process
                    .utility_temperature(recuperator_liquid.out())
                    .unwrap()
                    .to_reduced(t_out)?
                    - 1.0,
            );
        }

        Ok((process, target, constraints))
    }
}

#[derive(Clone, Deserialize, Debug)]
#[serde(from = "OrganicRankineCycleSuperStructureJSON")]
pub struct OrganicRankineCycleRecuperator {
    heat_source: Utility,
    pub min_heat_source_outlet_temperature: Option<SINumber>,
    isentropic_turbine_efficiency: f64,
    isentropic_pump_efficiency: f64,
    min_abs_pressure: SINumber,
    min_red_pressure: f64,
    max_abs_pressure: SINumber,
    max_red_pressure: f64,
    cooling: Utility,
    min_temperature_difference_recuperator: SINumber,
    min_heat_transfer_rate_recuperator: SINumber,
}

impl From<OrganicRankineCycleSuperStructureJSON> for OrganicRankineCycleRecuperator {
    fn from(orc: OrganicRankineCycleSuperStructureJSON) -> Self {
        Self {
            heat_source: orc.heat_source,
            min_heat_source_outlet_temperature: orc
                .min_heat_source_outlet_temperature
                .map(|t| t * CELSIUS),
            isentropic_turbine_efficiency: orc.isentropic_turbine_efficiency,
            isentropic_pump_efficiency: orc.isentropic_pump_efficiency,
            min_abs_pressure: orc.min_abs_pressure * BAR,
            min_red_pressure: orc.min_red_pressure,
            max_abs_pressure: orc.max_abs_pressure * BAR,
            max_red_pressure: orc.max_red_pressure,
            cooling: orc.cooling,
            min_temperature_difference_recuperator: orc.min_temperature_difference_recuperator
                * KELVIN,
            min_heat_transfer_rate_recuperator: orc.min_heat_transfer_rate_recuperator
                * KILO
                * WATT,
        }
    }
}

impl ProcessModel for OrganicRankineCycleRecuperator {
    fn variables(&self) -> Vec<[Option<f64>; 2]> {
        vec![
            // Mass flow rate
            [Some(0.0), None],
            // pressure condenser
            [
                Some(self.min_red_pressure.ln()),
                Some(self.max_red_pressure.ln()),
            ],
            // pressure evaporator
            [
                Some(self.min_red_pressure.ln()),
                Some(self.max_red_pressure.ln()),
            ],
            // superheating
            [Some(0.0), Some(1.0)],
            // heat flow recuperator
            [Some(0.0), None],
        ]
    }

    fn constraints(&self) -> Vec<[Option<f64>; 3]> {
        let mut constraints = vec![
            // Pinch constraints
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            // Absolute pressure (bar)
            [Some(0.0), Some(1.0), None],
            [Some(0.0), Some(1.0), None],
            // Recuperator
            [Some(0.0), None, None],
            [Some(0.0), None, None],
            [Some(0.0), None, None],
        ];
        // Heat source outlet
        if self.min_heat_source_outlet_temperature.is_some() {
            constraints.push([Some(0.0), None, None]);
        }
        constraints
    }

    fn solve<E: EquationOfState + MolarWeight<SIUnit>>(
        &self,
        eos: &Rc<E>,
        x: &[f64],
    ) -> EosResult<(Process<E>, f64, Vec<f64>)> {
        // unpack variables
        let mwf = x[0] * KILOGRAM / SECOND;
        let p_cond_red = x[1].exp();
        let p_evap_red = x[2].exp();
        let t_min = self
            .min_heat_source_outlet_temperature
            .unwrap_or(self.cooling.temperature);
        let dt_sh = x[3] * (self.heat_source.temperature - t_min);
        let q_rec = x[4] * MEGA * WATT;

        // Calculate pressures
        let cp = State::critical_point(eos, None, None, Default::default())?;
        let p_crit = cp.pressure(Contributions::Total);
        let p_cond = p_cond_red * p_crit;
        let p_evap = p_evap_red * p_crit;

        // Calculate isobars
        let isobar_cond = Isobar::new(eos, p_cond);
        let isobar_evap = Isobar::new(eos, p_evap);

        // Initialize process
        let mut process = Process::new();

        // Calculate pump
        let feed = process.add_state(ProcessState::SinglePhase(
            Box::new(isobar_cond.saturated_liquid()?.clone()),
            Some(mwf),
        ));
        let pump = process.pump(feed, &isobar_evap, self.isentropic_pump_efficiency)?;

        // Calculate turbine
        let feed = process.add_state(ProcessState::new_pure_temperature_pressure(
            eos,
            isobar_evap.saturated_vapor()?.temperature + dt_sh,
            p_evap,
            Some(mwf),
            DensityInitialization::Vapor,
        )?);
        let turbine = process.turbine(feed, &isobar_cond, self.isentropic_turbine_efficiency)?;

        // Calculate Recuperator
        let [recuperator_liquid, recuperator_vapor] =
            process.recuperator(pump.out(), turbine.out(), &isobar_evap, &isobar_cond, q_rec)?;

        // Calculate evaporator
        let evaporator = process.evaporator(recuperator_liquid.out(), &isobar_evap, Some(dt_sh))?;
        process.add_utility(&evaporator, self.heat_source);

        // Calculate condenser
        let condenser = process.total_condenser(recuperator_vapor.out(), &isobar_cond, None)?;
        process.add_utility(&condenser, self.cooling);

        // Target
        let target = process.net_power().unwrap().to_reduced(KILO * WATT)?;

        // Pinch constraints
        let mut constraints = process.pinch_constraints();

        // Absolute pressure constraints
        constraints
            .push((p_cond - self.min_abs_pressure).to_reduced(p_evap - self.min_abs_pressure)?);
        constraints.push((p_evap - p_cond).to_reduced(self.max_abs_pressure - p_cond)?);

        // Recuperator constraints
        constraints.push(
            (process[turbine.out()].temperature()
                - process[recuperator_liquid.out()].temperature())
            .to_reduced(self.min_temperature_difference_recuperator)?
                - 1.0,
        );
        constraints.push(
            (process[recuperator_vapor.out()].temperature() - process[pump.out()].temperature())
                .to_reduced(self.min_temperature_difference_recuperator)?
                - 1.0,
        );
        constraints.push(q_rec.to_reduced(self.min_heat_transfer_rate_recuperator)? - 1.0);

        // Heat source outlet constraint
        if let Some(t_out) = self.min_heat_source_outlet_temperature {
            constraints.push(
                process
                    .utility_temperature(recuperator_liquid.out())
                    .unwrap()
                    .to_reduced(t_out)?
                    - 1.0,
            );
        }

        Ok((process, target, constraints))
    }
}
